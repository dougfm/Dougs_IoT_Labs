

from flask import Flask
import socket

## get hostname
if socket.gethostname().find('.') >= 0:
    hostname=socket.gethostname()
else:
    hostname=socket.gethostbyaddr(socket.gethostname())[0]

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello IoT from dougspi: " + hostname

## run website
if __name__ == "__main__":
    app.run(host='0.0.0.0')


